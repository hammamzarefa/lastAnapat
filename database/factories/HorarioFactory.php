<?php

namespace Database\Factories;

use App\Models\Horario;
use Illuminate\Database\Eloquent\Factories\Factory;

class HorarioFactory extends Factory
{
    protected $model = Horario::class;

    public function definition()
    {
        return [
            "id"=>$this->faker->randomDigit(10),
            "curso"=>$this->faker->randomDigit(10),
            "contenido"=>$this->faker->randomElement(['Teoría', 'Práctica']),
            "fecha_inicio"=>$this->faker->date(),
            "final"=>$this->faker->date(),
            "alumnos"=>$this->faker->randomDigit(10),
            "tipo_maquina"=>$this->faker->randomDigit(10),
        ];
    }
}
