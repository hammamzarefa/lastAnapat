<?php

namespace Database\Factories;

use App\Models\Carnet;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarnetFactory extends Factory
{
    protected $model = Carnet::class;

    public function definition()
    {
        return [
            'id'=>$this->faker->unique()->randomDigit(2),
            'numero'=>$this->faker->name(),
            'operador'=>$this->faker->randomDigit(2),
            'foto'=>$this->faker->name(),
            'fecha_de_alta'=>$this->faker->date(),
            'fecha_de_emision'=>$this->faker->date(),
            // 'tipos_de_pemp'=>$this->faker->name(),
            'curso'=>$this->faker->randomDigit(2),
            'estado'=>$this->faker->boolean(),
            'examen_teorico_realizado'=>$this->faker->randomElement(['básico', 'Extendido']),
        ];
    }
}
