<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'perfil'=>$this->faker->randomElement(['Administrador','Formador','Responsable_de_Formacion']),
            'alias'=>$this->faker->name(),
            'apellidos'=>$this->faker->name(),
            'nombre'=>$this->faker->name(),
            'provincia'=>$this->faker->name(),
            'ciudad'=>$this->faker->name(),
            'direccion'=>$this->faker->name(),
            'codigo_postal'=>$this->faker->randomDigit(5),
            'entidad'=>$this->faker->randomDigit(5),
            'estado'=>$this->faker->boolean(),

        ];
    }
}
