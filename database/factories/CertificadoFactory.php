<?php

namespace Database\Factories;

use App\Models\Certificado;
use Illuminate\Database\Eloquent\Factories\Factory;

class CertificadoFactory extends Factory
{
    
    protected $model = Certificado::class;
    
    public function definition()
    {
        return [
            'numero'=>$this->faker->name(),
            'operador'=>$this->faker->randomDigit(2),
            'entidad'=>$this->faker->randomDigit(2),
            'curso'=>$this->faker->randomDigit(2),
            'emision'=>$this->faker->date(),
            'vencimiento'=>$this->faker->date(),
            'observaciones'=>$this->faker->name(),
            'cer_fecha'=>$this->faker->date(),
            'cer_apellidos'=>$this->faker->name(),
            'cer_nombre'=>$this->faker->name(),
            'dni'=>$this->faker->name(),
            'cer_type_course'=>$this->faker->name(),
            'fecha_alta'=>$this->faker->date(),
            'entidad_nombre'=>$this->faker->name(),
            'tipos_carnet'=>$this->faker->name(),
            'carnet'=>$this->faker->name(),
            'tipo_1'=>$this->faker->name(),
            'tipo_2'=>$this->faker->name(),
            'tipo_3'=>$this->faker->name(),
            'tipo_4'=>$this->faker->name(),

        ];
    }
}
