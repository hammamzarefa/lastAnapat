<?php

namespace Database\Factories;

use App\Models\General;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class GeneralFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = General::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name(),
            'favicon' => $this->faker->text(),
            'logo' => $this->faker->text(),
            'address1' => $this->faker->text(),

            'address2' => $this->faker->text(),
            'phone' => $this->faker->text(),
            'email' => $this->faker->text(),
            'twitter' => $this->faker->text(),
            'instagram' => $this->faker->text(),
            'linkedin' => $this->faker->text(),
            'footer' => $this->faker->text(),
            'gmaps' => $this->faker->text(),
            'tawkto' => $this->faker->text(),
            'disqus' => $this->faker->text(),
            'gverification' => $this->faker->text(),
            'sharethis' => $this->faker->text(),
            'keyword' => $this->faker->text(),
            'meta_desc' => $this->faker->text(),
            

        ];
    }
}
