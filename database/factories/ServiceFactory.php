<?php

namespace Database\Factories;

use App\Models\Service;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServiceFactory extends Factory
{
    protected $model = Service::class;
   
    public function definition()
    {
        return [
            'icon'=> $this->faker->name(),
            'title'=>$this->faker->name(),
            'slug'=>$this->faker->name(),
            'quote'=>$this->faker->text(),
            'desc'=>$this->faker->text(),
        ];
    }
}
