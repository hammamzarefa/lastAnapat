<?php

namespace Database\Factories;

use App\Models\Cursos;
use Illuminate\Database\Eloquent\Factories\Factory;

class cursosFactory extends Factory
{
    protected $model = Cursos::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'curso'=>$this->faker->randomDigit(11),
            'tipo_curso'=>$this->faker->randomDigit(11),
            'tipo_maquina_1'=>$this->faker->randomDigit(11),
            'tipo_maquina_2'=>$this->faker->randomDigit(11),
            'tipo_maquina_3'=>$this->faker->randomDigit(11),
            'tipo_maquina_4'=>$this->faker->randomDigit(11),
            'codigo'=>$this->faker->name,
            'entidad'=>$this->faker->randomDigit(11),
            'formador'=>$this->faker->randomDigit(11),
            'formador_apoyo_1'=>$this->faker->randomDigit(11),
            'formador_apoyo_2'=>$this->faker->randomDigit(11),
            'formador_apoyo_3'=>$this->faker->randomDigit(11),
            'fecha_inicio'=>$this->faker->date,
            'direccion'=>$this->faker->name,
            'ciudad'=>$this->faker->name,
            'provincia'=>$this->faker->name,
            'codigo_postal'=>$this->faker->name,
            'examen_t'=>$this->faker->randomDigit(11),
            'examen_p'=>$this->faker->randomDigit(11),
            'asistentes_pdf'=>$this->faker->name,
            'fecha_alta'=>$this->faker->date,
            'observaciones'=>$this->faker->name,
            'publico_privado'=>$this->faker->boolean,
            'cerrado'=>$this->faker->boolean,
            'estado'=>$this->faker->boolean,

        ];
    }
}
