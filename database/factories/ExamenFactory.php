<?php

namespace Database\Factories;

use App\Models\Examen;
use Illuminate\Database\Eloquent\Factories\Factory;

class ExamenFactory extends Factory
{
    protected $model = Examen::class;

    public function definition()
    {
        return [
            "id"=>$this->faker->randomDigit(),
            "codigo"=>$this->faker->name(),
            "tipo"=>$this->faker->randomElement(['T-Theoretical', 'P-Practical']),
            "nombre"=>$this->faker->name(),
            "url"=>$this->faker->name(),
        ];
    }
}
