<?php

namespace Database\Factories;

use App\Models\Link;
use Illuminate\Database\Eloquent\Factories\Factory;

class LinkFactory extends Factory
{
    protected $model = Link::class;

    public function definition()
    {
        return [
            // 'id'=>$this->faker->randomDigit(10),
            'title'=>$this->faker->name(),
            'slug'=>$this->faker->name(),
            'text'=>$this->faker->name(),

        ];
    }
}
