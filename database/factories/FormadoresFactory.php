<?php

namespace Database\Factories;

use App\Models\Formadores;
use Illuminate\Database\Eloquent\Factories\Factory;

class FormadoresFactory extends Factory
{
    protected $model =  Formadores::class;

    public function definition()
    {
        return [
            'id'=>$this->faker->unique()->randomDigit(10),
            'codigo'=>$this->faker->name(),
            'entidad'=>$this->faker->randomDigit(),
            'apellidos'=>$this->faker->name(),
            'nombre'=>$this->faker->name(),
            'dni'=>$this->faker->name(),
            'dni_img'=>$this->faker->name(),
            'operador_pdf'=>$this->faker->name(),
            'cert_empresa_pdf'=>$this->faker->name(),
            'vida_laboral_pdf'=>$this->faker->name(),
            'prl_pdf'=>$this->faker->name(),
            'pemp_pdf'=>$this->faker->name(),
            'cap_pdf'=>$this->faker->name(),
            'fecha'=>$this->faker->date(),
            'estado'=>$this->faker->boolean(),

        ];
    }
}
