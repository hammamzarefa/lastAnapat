<?php

namespace Database\Factories;

use App\Models\Tipo_Maquina;
use Illuminate\Database\Eloquent\Factories\Factory;

class Tipo_MaquinaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model= Tipo_Maquina::class;

    public function definition()
    {
        return [
            'id'=>$this->faker->randomDigit(5),
            'tipo_maquina'=>$this->faker->text,
        ];
    }
}
