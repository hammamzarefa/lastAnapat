<?php

namespace Database\Factories;

use App\Models\Testimonial;
use Illuminate\Database\Eloquent\Factories\Factory;
use Symfony\Component\Console\Terminal;

class TestimonialFactory extends Factory
{
    protected $model = Testimonial::class;

    public function definition()
    {
        return [
            'photo'=>$this->faker->text,
            'name'=>$this->faker->text,
            'profession'=>$this->faker->text,
            'desc'=>$this->faker->text,
            'status'=> $this->faker->randomElement(['PUBLISH', 'DRAFT']),

        ];
    }
}
