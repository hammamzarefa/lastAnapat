<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
   protected $model = Post::class;

    public function definition()
    {
        return [
            'category_id'=>$this->faker->randomDigit(),
            'author_id'=>$this->faker->randomDigit(),
            'title'=>$this->faker->name(),
            'slug'=>$this->faker->name(),
            'body'=>$this->faker->text(),
            'cover'=>$this->faker->name(),
            'keyword'=>$this->faker->name(),
            'meta_desc'=>$this->faker->name(),
            'views'=>$this->faker->randomDigit(),
            'status'=>$this->faker->randomElement(['PUBLISH', 'DRAFT']),
            'public'=>$this->faker->boolean(),

        ];
    }
}
