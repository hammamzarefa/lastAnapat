<?php

namespace Database\Factories;

use App\Models\Portfolio;
use Illuminate\Database\Eloquent\Factories\Factory;

class PortfolioFactory extends Factory
{
    
    protected $model = Portfolio::class;

    public function definition()
    {
        return [
            'pcategory_id'=>$this->faker->randomDigit(),
            'name'=>$this->faker->name(),
            'slug'=>$this->faker->name(),
            'cover'=>$this->faker->name(),
            'client'=>$this->faker->name(),
            'date'=>$this->faker->date(),
            'desc'=>$this->faker->text(),
        ];
    }
}
