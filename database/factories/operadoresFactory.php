<?php

namespace Database\Factories;

use App\Models\Operadores;
use Illuminate\Database\Eloquent\Factories\Factory;

class operadoresFactory extends Factory
{
    protected $model = Operadores::class;

    public function definition()
    {
        return [
            'id'=>$this->faker->randomDigit(11),
            'dni'=>$this->faker->name(),
            'apellidos'=>$this->faker->name(),
            'nombre'=>$this->faker->name(),
            'entidad'=>$this->faker->randomDigit(2),
            'foto'=>$this->faker->name(),
            'dni_img'=>$this->faker->name(),
            'fecha_nacimiento'=>$this->faker->date(),
            'provincia'=>$this->faker->name(),
            'ciudad'=>$this->faker->name(),
            'direccion'=>$this->faker->name(),
            'codigo_postal'=>$this->faker->name(),
            'mail'=>$this->faker->name(),
            'carnet'=>$this->faker->name(),
            'fecha'=>$this->faker->date(),
            'estado'=>$this->faker->boolean(),

        ];
    }
}
