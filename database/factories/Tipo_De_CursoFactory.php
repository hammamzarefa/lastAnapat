<?php

namespace Database\Factories;

use App\Models\Tipo_De_Curso;
use Illuminate\Database\Eloquent\Factories\Factory;

class Tipo_De_CursoFactory extends Factory
{
    protected $model =Tipo_De_Curso::class;

    public function definition()
    {
        return [
            'id'=>$this->faker->randomDigit(),
            "tipo_curso"=>$this->faker->name(),
         	"codigo"=>$this->faker->randomDigit(),
        ];
    }
}
