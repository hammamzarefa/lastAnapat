<?php

namespace Database\Factories;

use App\Models\Partner;
use Illuminate\Database\Eloquent\Factories\Factory;

class PartnerFactory extends Factory
{
    protected $model = Partner::class;

    public function definition()
    {
        return [
            'cover'=>$this->faker->text,
            'name'=>$this->faker->text,
            'link'=>$this->faker->text,
            'empresa'=>$this->faker->text,
            'direccion'=>$this->faker->text,
            'codigo_postal'=>$this->faker->text,
            'poblacion'=>$this->faker->text,
            'provincia'=>$this->faker->text,
            'telefono'=>$this->faker->text,
            'email'=>$this->faker->text,

        ];
    }
}
