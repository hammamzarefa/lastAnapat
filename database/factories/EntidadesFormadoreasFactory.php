<?php

namespace Database\Factories;

use App\Models\EntidadesFormadoreas;
use Illuminate\Database\Eloquent\Factories\Factory;

class EntidadesFormadoreasFactory extends Factory
{
    protected $model = EntidadesFormadoreas::class;

    public function definition()
    {
        return [
            'id'=>$this->faker->unique(false,100),
            'socio'=>$this->faker->randomDigit(11),
            'cif'=>$this->faker->text,
            'nombre'=>$this->faker->name(),
            'razon_social'=>$this->faker->text,
            'province'=>$this->faker->text,
            'ciudad'=>$this->faker->text,
            'direccion'=>$this->faker->text,
            'codigo_postal'=>$this->faker->text,
            'logo'=>$this->faker->text,
            'web'=>$this->faker->text,
            'mail'=>$this->faker->text,
            'doc_medios_pdf'=>$this->faker->text,
            'fecha'=>$this->faker->date,
            'estado'=>$this->faker->boolean,
            'certificado'=>$this->faker->boolean,

        ];
    }
}
