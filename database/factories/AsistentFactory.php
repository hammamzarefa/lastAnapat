<?php

namespace Database\Factories;

use App\Models\Asistent;
use Illuminate\Database\Eloquent\Factories\Factory;

class AsistentFactory extends Factory
{
    protected $model = Asistent::class;

    public function definition()
    {
        return [
            'curso'=>$this->faker->randomDigit(),
            'orden'=>$this->faker->randomDigit(),
            'operador'=>$this->faker->randomDigit(),
            'tipo_carnet'=>$this->faker->randomDigit(),
            'nota_t'=>$this->faker->randomFloat(),
            'nota_p'=>$this->faker->randomDigit(),
            'examen_t_pdf'=>$this->faker->name(),
            'examen_p_pdf'=>$this->faker->name(),
            'tipo_1'=>$this->faker->randomDigit(),
            'tipo_2'=>$this->faker->randomDigit(),
            'tipo_3'=>$this->faker->randomDigit(),
            'tipo_4'=>$this->faker->randomDigit(),
            'examen_t_pdf'=>$this->faker->date(),
            'vencimiento'=>$this->faker->date(),
            'observaciones'=>$this->faker->name(),
            'rememberToken'=>$this->faker->dateTime(),
            'tipos_carnet'=>$this->faker->name(),

        ];
    }
}
