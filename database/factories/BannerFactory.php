<?php

namespace Database\Factories;

use App\Models\About;
use App\Models\Banner;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BannerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Banner::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'cover' => $this->faker->name,
            'title' => $this->faker->text,
            'desc' => $this->faker->text,
            'link' => $this->faker->text,

        ];
    }
}
