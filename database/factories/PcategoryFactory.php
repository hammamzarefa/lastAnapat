<?php

namespace Database\Factories;

use App\Models\Pcategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class PcategoryFactory extends Factory
{
   protected $model= Pcategory::class;

    public function definition()
    {
        return [
            'name'=>$this->faker->name()
        ];
    }
}
