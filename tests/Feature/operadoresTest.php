<?php

namespace Tests\Feature;


use App\Http\Controllers\FrontController;
use App\Models\About;
use App\Models\Asistent;
use App\Models\Banner;
use App\Models\Carnet;
use App\Models\Category;
use App\Models\Certificado;
use App\Models\Cursos;
use App\Models\EntidadesFormadoreas;
use App\Models\Faq;
use App\Models\Formadores;
use App\Models\General;
use App\Models\Horario;
use App\Models\Link;
use App\Models\Operadores;
use App\Models\Page;
use App\Models\Partner;
use App\Models\Pcategory;
use App\Models\Portfolio;
use App\Models\Post;
use App\Models\Service;
use App\Models\Subscriber;
use App\Models\Tag;
use App\Models\Testimonial;
use App\Models\Tipo_Maquina;
use App\Models\User;
use Carbon\Carbon;
use Database\Factories\cursosFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\ToArray;
use Tests\TestCase;

class operadoresTest extends TestCase
{
    use RefreshDatabase;

    private $General;
    private $Banner;
    private $faq;
    private $partners;
    private $Page;
    private $Testimonial;
    private $Cursos;
    private $EntidadesFormador;
    private $Tipo_Maquina;
    private $service;
    private $pcategory;
    private $portfolios;
    private $Category;
    private $Link;
    private $user;
    private $formadores;
    private $about;
    private $horario;
    private $operadores;
    private $Certificado;

    public function setUp():void{
        parent::setUp();
        
        $this->General = General::factory(1)->create(['id'=>1]); 
        $this->Banner = Banner::factory(1)->create();     
        $this->faq = Faq::factory(1)->create();     
        $this->Page = Page::factory(1)->create();     
        $this->partners = Partner::factory(1)->create();     
        $this->Testimonial = Testimonial::factory(1)->create();
        $this->EntidadesFormador = EntidadesFormadoreas::factory(1)->create(['id'=>2]);
        $this->Cursos = Cursos::factory(1)->create(['entidad'=>2]);
        $this->Tipo_Maquina = Tipo_Maquina::factory(1)->create(); 
        $this->service = Service::factory(1)->create();
        $this->pcategory = Pcategory::factory(1)->create();
        $this->portfolios = Portfolio::factory(1)->create();
        $this->Category = Category::factory(1)->create();
        $this->user = User::factory(12)->create();
        $this->horario = Horario::factory(1)->create();
        $this->Link = Link::factory(4)->create();
        $this->Certificado = Certificado::factory(1)->create();
        // $this->operadores = Operadores::factory(2)->create();
        // $this->about = About::factory(1)->create();
        // $this->formadores = Formadores::factory(1)->create();

    }

    public function test_index()
    {
        $user = User::factory(12)->create();
       $Operadores =  Operadores::factory(1)->create(['entidad'=> $user[0]->entidad]);

        $response = $this->actingAs($user[0])->get('admin/operadores');

        $response->assertStatus(200);

        $response->assertViewHas('operadores', function ($collection) use($Operadores){
            return $collection->contains($Operadores[0]);
        });
    }

    public function test_create(){
        $user = User::factory(12)->create(['perfil'=>'Administrador']);
        $Operadores =  Operadores::factory(1)->create();

        $response = $this->actingAs($user[0])->get('admin/operadores/create');

        $response->assertStatus(200);

        $response->assertViewHas('now');
        $response->assertViewHas('entidad', function ($collection) use($Operadores){
            return $collection->contains($this->EntidadesFormador[0]);
        });
    }

    public function test_story(){
        Storage::fake('logo');

        $user = User::factory(12)->create();
        $response = $this->actingAs($user[0])->post('admin/operadores/create',[
            'dni' => 'required',
            'apellidos' => 'required',
            'nombre' => 'required',
            'entidad' => '10',
            'codigo_postal' => 'max',
            'foto' => UploadedFile::fake()->create('logo.jpg'),
            'dni_img' => UploadedFile::fake()->create('logo.jpg'),
        ]);

        $response->assertStatus(302);

        $response->assertSessionHas('success', 'Data added successfully');
    }

    public function test_show(){
        $user = User::factory(1)->create(['perfil'=>'Administrador']);
        $Operadores =  Operadores::factory(1)->create(['nombre'=>'José Antonio','entidad'=>$this->EntidadesFormador[0]]);
        $response = $this->actingAs($user[0])->get('admin/operadores/show/'.$Operadores[0]->id );
        $response->assertStatus(200);
        $response->assertViewHas('operador');
    }

    public function test_certificado(){
        $user = User::factory(1)->create(['perfil'=>'Administrador']);
        $Operadores =  Operadores::factory(1)->create(['nombre'=>'José Antonio','entidad'=>$this->EntidadesFormador[0]]);
        $asistent = Asistent::factory(1)->create(['operador'=>$Operadores[0]->id,'curso'=>$this->Cursos[0]->id]);
        $Certificado = Certificado::factory(1)->create(['operador'=>$Operadores[0]->id,'curso'=>$this->Cursos[0]->id]);

        $response = $this->actingAs($user[0])->get('admin/operadores/certificado/'.$Operadores[0]->id );
        $response->assertStatus(200);
        $response->assertViewHas(['operador', 'curso', 'tipos', 'cert_numero', 'activeAsistent','certific']);
        $response->assertViewHas('certific', function ($collection) use($Certificado){
            return $collection->contains($Certificado[0]);
        });

    }

    public function test_edit(){
        $EntidadesFormadoreas= EntidadesFormadoreas::factory(1)->create(['id'=>3]);
        $user = User::factory(1)->create(['entidad'=>$EntidadesFormadoreas[0]->id]);
        $Operadores =  Operadores::factory(1)->create(['nombre'=>'José Antonio','entidad'=>$EntidadesFormadoreas[0]->id]);

        $response = $this->actingAs($user[0])->get('admin/operadores/edit/'.$Operadores[0]->id );
        $response->assertStatus(200);
        $response->assertViewHas('operadores');
        $response->assertViewHas('entidad');

    }

    public function test_update(){
        Storage::fake('logo');

        $user = User::factory(1)->create(['entidad'=>$this->EntidadesFormador[0]->id]);
        $Operadores =  Operadores::factory(1)->create(['nombre'=>'José Antonio','entidad'=>$this->EntidadesFormador[0]->id]);

        $response = $this->actingAs($user[0])->post('admin/operadores/edit/'.$Operadores[0]->id,[
            'dni' => 'test',
            'apellidos' => 'test',
            'nombre' => 'test',
            'entidad' => 'required',
            'codigo_postal' => '1',
            'foto' => UploadedFile::fake()->create('logo.jpg'),
            'dni_img' => UploadedFile::fake()->create('logo.jpg'),
        ] );
        $response->assertStatus(302);
        $response->assertSessionHas('success', 'Data added successfully');
    }

    public function test_destroy(){
        $user = User::factory(1)->create();
        $Operadores =  Operadores::factory(1)->create(['nombre'=>'José Antonio','entidad'=>$this->EntidadesFormador[0]->id]);

        $response = $this->actingAs($user[0])->delete('admin/operadores/destroy/'.$Operadores[0]->id);
        $response->assertStatus(302);
        $response->assertSessionHas('success', 'Data deleted successfully');
    }


}
