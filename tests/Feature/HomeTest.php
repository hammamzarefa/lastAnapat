<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HomeTest extends TestCase
{
    use RefreshDatabase;

    public function test_index()
    {
        $user = User::factory(1)->create();

        $response = $this->actingAs($user[0])->get('/home');

        $response->assertStatus(200);
    }
}
