<?php

namespace Tests\Feature;


use App\Http\Controllers\FrontController;
use App\Models\About;
use App\Models\Banner;
use App\Models\Carnet;
use App\Models\Category;
use App\Models\Certificado;
use App\Models\Cursos;
use App\Models\EntidadesFormadoreas;
use App\Models\Faq;
use App\Models\Formadores;
use App\Models\General;
use App\Models\Horario;
use App\Models\Link;
use App\Models\Operadores;
use App\Models\Page;
use App\Models\Partner;
use App\Models\Pcategory;
use App\Models\Portfolio;
use App\Models\Post;
use App\Models\Service;
use App\Models\Subscriber;
use App\Models\Tag;
use App\Models\Testimonial;
use App\Models\Tipo_Maquina;
use App\Models\User;
use Carbon\Carbon;
use Database\Factories\cursosFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\ToArray;
use Tests\TestCase;

class HorarioTest extends TestCase
{
    use RefreshDatabase;

    private $General;
    private $Banner;
    private $faq;
    private $partners;
    private $Page;
    private $Testimonial;
    private $Cursos;
    private $EntidadesFormador;
    private $Tipo_Maquina;
    private $service;
    private $pcategory;
    private $portfolios;
    private $Category;
    private $Link;
    private $user;
    private $formadores;
    private $about;
    private $horario;

    public function setUp():void{
        parent::setUp();
        
        $this->General = General::factory(1)->create(['id'=>1]); 
        $this->Banner = Banner::factory(1)->create();     
        $this->faq = Faq::factory(1)->create();     
        $this->Page = Page::factory(1)->create();     
        $this->partners = Partner::factory(1)->create();     
        $this->Testimonial = Testimonial::factory(1)->create();
        $this->EntidadesFormador = EntidadesFormadoreas::factory(1)->create(['id'=>2]);
        $this->Cursos = Cursos::factory(1)->create(['entidad'=>2]);
        $this->Tipo_Maquina = Tipo_Maquina::factory(1)->create(); 
        $this->service = Service::factory(1)->create();
        $this->pcategory = Pcategory::factory(1)->create();
        $this->portfolios = Portfolio::factory(1)->create();
        $this->Category = Category::factory(1)->create();
        $this->user = User::factory(12)->create();
        $this->horario = Horario::factory(1)->create();
        // $this->about = About::factory(1)->create();
        // $this->formadores = Formadores::factory(1)->create();

    }
    public function test_index()
    {
        $user = User::factory(1)->create();

        $response = $this->actingAs($user[0])->get('admin/horario');

        $response->assertStatus(200);
        $response->assertViewHas("horario");
        $response->assertViewHas('horario', function ($collection)  {
            return $collection->contains($this->horario[0]);
        });

    }

    public function test_create(){
        $user = User::factory(1)->create();

        $response = $this->actingAs($user[0])->get('admin/horario/create/'.$this->Cursos[0]->id);

        $response->assertStatus(200);
        $response->assertViewHas('curso', function ($collection)  {
            return $collection->contains($this->Cursos[0]);
        });
        $response->assertViewHas('tipo_maq', function ($collection)  {
            return $collection->contains($this->Tipo_Maquina[0]);
        });
        $response->assertViewHas('tipos');
        $response->assertViewHas('id');
    }

    public function test_store(){
        $user = User::factory(1)->create();
        $response = $this->actingAs($user[0])->post('admin/horario/create',[
            'curso' => $this->Cursos[0]->id,
            'contenido' => "Teoría",
            'fecha_inicio' => $this->horario[0]->fecha_inicio,
            'final' => $this->horario[0]->final,
            'alumnos' => $this->horario[0]->alumnos,
            "tipo_maquina"=>1,
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('cursos','entidad','formador','tipo_maquina','tipo_curso','examen_t','examen_p','formadors','formadors2','formadors3');

    }

    public function test_update(){
        $user = User::factory(1)->create();
        $response = $this->actingAs($user[0])->post('admin/horario/edit/'. $this->horario[0]->id,[
            'curso' => $this->Cursos[0]->id,
            'contenido' => "Teoría",
            'fecha_inicio' => $this->horario[0]->fecha_inicio,
            'final' => $this->horario[0]->final,
            'alumnos' => $this->horario[0]->alumnos,
            "tipo_maquina"=>1,
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('success', 'Data updated successfully');

    }

    public function test_edit(){
        $user = User::factory(1)->create();
        $horior = Horario::factory(1)->create(['id'=>1,'curso'=>$this->Cursos[0]->id]);
        $response = $this->actingAs($user[0])->get('admin/horario/edit/'.$horior[0]->id);
        $response->assertStatus(200);

        $response->assertViewHas(['curso','tipo_maq']);
        $response->assertViewHas('curso', function ($collection)  {
            return $collection->contains($this->Cursos[0]);
        });
        $response->assertViewHas('tipo_maq', function ($collection)  {
            return $collection->contains($this->Tipo_Maquina[0]);
        });

    }


    public function test_destroy(){
        $user = User::factory(1)->create();
        $horior = Horario::factory(1)->create(['id'=>1,'curso'=>$this->Cursos[0]->id]);
        $response = $this->actingAs($user[0])->delete('admin/horario/destroy/'.$horior[0]->id);

        $response->assertStatus(302);

    }
}
