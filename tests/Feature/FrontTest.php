<?php

namespace Tests\Feature;

use App\Http\Controllers\FrontController;
use App\Models\Banner;
use App\Models\Carnet;
use App\Models\Category;
use App\Models\Certificado;
use App\Models\Cursos;
use App\Models\EntidadesFormadoreas;
use App\Models\Faq;
use App\Models\General;
use App\Models\Link;
use App\Models\Operadores;
use App\Models\Page;
use App\Models\Partner;
use App\Models\Pcategory;
use App\Models\Portfolio;
use App\Models\Post;
use App\Models\Service;
use App\Models\Subscriber;
use App\Models\Tag;
use App\Models\Testimonial;
use App\Models\Tipo_Maquina;
use App\Models\User;
use Carbon\Carbon;
use Database\Factories\cursosFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Maatwebsite\Excel\Concerns\ToArray;
use Tests\TestCase;

class FrontTest extends TestCase
{
    use RefreshDatabase;

    private $General;
    private $Banner;
    private $faq;
    private $partners;
    private $Page;
    private $Testimonial;
    private $Cursos;
    private $EntidadesFormador;
    private $Tipo_Maquina;
    private $service;
    private $pcategory;
    private $portfolios;
    private $Category;
    private $Link;
    private $user;

    public function setUp():void{
        parent::setUp();
        
        $this->General = General::factory(1)->create(['id'=>1]); 
        $this->Banner = Banner::factory(1)->create();     
        $this->faq = Faq::factory(1)->create();     
        $this->Page = Page::factory(1)->create();     
        $this->partners = Partner::factory(1)->create();     
        $this->Testimonial = Testimonial::factory(1)->create();
        $this->EntidadesFormador = EntidadesFormadoreas::factory(1)->create(['id'=>2]);
        $this->Cursos = Cursos::factory(1)->create(['entidad'=>2]);
        $this->Tipo_Maquina = Tipo_Maquina::factory(1)->create(); 
        $this->service = Service::factory(1)->create();
        $this->pcategory = Pcategory::factory(1)->create();
        $this->portfolios = Portfolio::factory(1)->create();
        $this->Category = Category::factory(1)->create();
        $this->user = User::factory(1)->create();

    }



//------- test front.home paget -------//  
    public function test_home()
    {     
        $response = $this->get('/');
        $response->assertStatus(200);

        $response->assertViewHas('general');
        $response->assertViewHas('banner', function ($collection) {
            return $collection->contains($this->Banner[0]);
        });
        $response->assertViewHas('faqs', function ($collection) {
            return $collection->contains($this->faq[0]);
        });
        $response->assertViewHas('pages');
        $response->assertViewHas('partner', function ($collection) {
            return $collection->contains($this->partners[0]);
        });
    }

//------ test front.about page ------//
    public function test_about()
    {        
        General::factory(1)->create(); 

        $response = $this->get('/about-us');
        $response->assertStatus(200);
        $response->assertViewHas('general');
    }

//------ test front.quienes_somos (about2 function)page ------// 
    public function test_quienes_somaos()
    {    
        $response = $this->get('/quienes-somos');
        $response->assertStatus(200);
        $response->assertViewHas('general');
    }

//------ test front.cotact page ------// 
    public function test_contact()
    {        
        $response = $this->get('/contact');
        $response->assertStatus(200);
        $response->assertViewHas('general');
    }

//------ test front.testi page ------// 
    public function test_testi()
    {
        $response = $this->get('/testimonials');
        $response->assertStatus(200);
        $response->assertViewHas('general');
        $response->assertViewHas('testi', function ($collection) {
            return $collection->contains($this->Testimonial[0]);
        });    
    }

//------ test front.cursos page ------// 
    public function test_cursos()
    {    
        $response = $this->get('/cursos');
        $response->assertStatus(200);
          
        $response->assertViewHas('general');
        $response->assertViewHas('cursos');
        $response->assertViewHas('entidades');
        
    }

//------ test front.curso page ------// 
    public function test_curso()
    {         
        $response = $this->get('curso/'. $this->Cursos[0]->curso);
        $response->assertStatus(200);
        
        $response->assertViewHas('general');
        $response->assertViewHas('curso');
        $response->assertViewHas('tipo');
    }

//------ test front.curso faild Cursos page ------// 
    public function test_faildCursos_curso()
    {      
        $response = $this->get('curso/'. ($this->Cursos[0]->curso+1));
        $response->assertStatus(404);
    }

//------ test front.curso faild EntidadesFormadoreas page ------// 
    public function test_faildCursos_EntidadesFormadoreas()
    {      
        $Cursos = Cursos::factory(1)->create(['entidad'=>4]);
        $response = $this->get('curso/'. 44);
        $response->assertStatus(404);
    }

    public function test_carnets()
    {        
        $response = $this->get('carnets');
        $response->assertStatus(200);
        
        $response->assertViewHas('general');
        $response->assertViewHas('test');
    }


//------ test front.carnets page  whereDate('fecha_de_alta' , '<' ,NOW() ) and ('fecha_de_emision' , '<=' ,NOW()) ------// 
    public function test_searchcarnet_fecha_de_alta_before_now_and_fecha_de_emision_before_now()
    {
        Carnet::factory(1)->create(['numero'=>'B008242','fecha_de_alta'=>Carbon::now()->subMonth(),'fecha_de_emision'=>Carbon::now()->subYear()]);

        $response = $this->post('searchcarnet',['numero'=>'B008242']);

        $response->assertStatus(200);
        
        $response->assertViewHas('general');
        $response->assertViewHas('test',"Esta Carné ya expiró.");
        
    }


    //------ test front.carnets page  whereDate('fecha_de_alta' , '>' ,NOW() ) and ('fecha_de_emision' , '<=' ,NOW()) ------// 
    public function test_searchcarnet_fecha_de_alta_bigger_now()
    {
        $Carnet = Carnet::factory(1)->create(['curso'=>5,'numero'=>'B008243','fecha_de_alta'=>Carbon::now()->addYear(),'fecha_de_emision'=>Carbon::now()->subYear()]);

        Certificado::factory(1)->create(['carnet'=>'B008243']);
        
        Operadores::factory(1)->create(['id'=>$Carnet[0]->operador]);
        
        Cursos::factory(1)->create(['id'=>$Carnet[0]->curso]);

        $response = $this->post('searchcarnet',['numero'=>'B008243']);
        $response->assertStatus(200);  
        $response->assertViewHas('general');
        $response->assertViewHas('operador');
        $response->assertViewHas('carnet');
        $response->assertViewHas('curso');
        $response->assertViewHas('certificado');
    }

     //------ test front.carnets page  whereDate('fecha_de_alta' , '>' ,NOW() ) and ('fecha_de_emision' , '<=' ,NOW()) ------// 
     public function test_searchcarnet_fecha_de_emision_before_now_and_fecha_de_emision_bigger_now()
     {
         $Carnet = Carnet::factory(1)->create(['curso'=>5,'numero'=>'B008243','fecha_de_alta'=>Carbon::now()->subMonth(),'fecha_de_emision'=>Carbon::now()->addYear()]);
 
         Certificado::factory(1)->create(['carnet'=>'B008243']);
         
         Operadores::factory(1)->create(['id'=>$Carnet[0]->operador]);
         
         Cursos::factory(1)->create(['id'=>$Carnet[0]->curso]);
 
         $response = $this->post('searchcarnet',['numero'=>'B008243']);
         $response->assertStatus(200);  
         $response->assertViewHas('general');
         
         $response->assertViewHas('test','Ningún Carné coincide con el código buscado.');
     }

    public function test_carnet(){
        $Carnet = Carnet::factory(1)->create(['id'=>3]);
 
        Certificado::factory(1)->create();
        
        $operador = Operadores::factory(1)->create(['id'=>$Carnet[0]->operador]);


        $response = $this->get('carnet/'. $Carnet[0]->id);
        $response->assertStatus(200);  
        $response->assertViewHas('general');
        $response->assertViewHas('operador');
        $response->assertViewHas('carnet');
        $response->assertViewHas('certificado');
    }    

    public function test_partners(){
        $response = $this->get('partners');
        $response->assertStatus(200);
        
        $response->assertViewHas('general');
        $response->assertViewHas('partners');
    }

    public function test_partner(){
        $response = $this->get('partner/'.$this->partners[0]->id);
        $response->assertStatus(200);
        
        $response->assertViewHas('general');
        $response->assertViewHas('partner');
    }

    public function test_entidades_formadoras(){
        Post::factory(3)->create(['status'=>'PUBLISH','public'=>1]);

        $response = $this->get('entidades_formadoras');
        $response->assertStatus(200);
        
        $response->assertViewHas(['general','entidadesFormadores','service','link','lpost','pcategories','portfolio']);
    }

    public function test_entidades_formadora(){
        Post::factory(3)->create(['status'=>'PUBLISH','public'=>1]);

        $response = $this->get('entidade_formadora/'.$this->EntidadesFormador[0]->id);
        $response->assertStatus(200);
        
        $response->assertViewHas(['general','entidadesFormadore','link','lpost']);
    }

    public function test_blug(){
        $posts = Post::factory(6)->create(['status'=>'PUBLISH','public'=>1,'author_id'=>$this->user[0]->id]);
        Tag::factory(1)->create();

        $response = $this->get('blog');
        $response->assertStatus(200);
        $response->assertViewHas(['categories','general','link','lpost','posts','recent','tags']);
    }

    public function test_blug_auth(){
        $posts = Post::factory(6)->create(['status'=>'PUBLISH','public'=>1,'author_id'=>$this->user[0]->id]);
        Tag::factory(1)->create();

        $response = $this->actingAs($this->user[0])->get('blog');
        $response->assertStatus(200);
        $response->assertViewHas(['categories','general','link','lpost','posts','recent','tags']);
    }

    public function test_blogshow(){
        $posts = Post::factory(6)->create(['status'=>'PUBLISH','public'=>1,'author_id'=>$this->user[0]->id]);
        Tag::factory(1)->create();

        $response = $this->get('blog/'. $posts[0]->slug);
        $response->assertStatus(200);
        $response->assertViewHas(['categories','general','link','lpost','post','recent','tags']);
    }

    public function test_blogshow_auth(){
        $posts = Post::factory(6)->create(['status'=>'PUBLISH','public'=>1,'author_id'=>$this->user[0]->id]);
        Tag::factory(1)->create();

        $response = $this->actingAs($this->user[0])->get('blog/'. $posts[0]->slug);
        $response->assertStatus(200);
        $response->assertViewHas(['categories','general','link','lpost','post','recent','tags']);
    }

    public function test_category(){
        Tag::factory(1)->create();
        Link::factory(1)->create();
        Category::factory(1)->create();
        $posts = Post::factory(6)->create(['status'=>'PUBLISH','public'=>1,'category_id'=>$this->Category[0],'author_id'=>$this->user[0]->id]);

        $response = $this->actingAs($this->user[0])->get('categories/'. $this->Category[0]->slug);
        $response->assertStatus(200);
        $response->assertViewHas(['categories','general','link','lpost','posts','recent','tags']);
    }

    public function test_privadoCategory_auth(){
        Tag::factory(1)->create();
        Link::factory(1)->create();
        Category::factory(1)->create();
        $posts = Post::factory(6)->create(['status'=>'PUBLISH','public'=>1,'category_id'=>$this->Category[0],'author_id'=>$this->user[0]->id]);

        $response = $this->actingAs($this->user[0])->get('privadoCategories/'. $this->Category[0]->slug);
        $response->assertStatus(200);
        $response->assertViewHas(['categories','general','link','lpost','posts','recent','tags']);
  
    }

    public function test_privadoCategory(){
        Tag::factory(1)->create();
        Link::factory(1)->create();
        Category::factory(1)->create();
        $posts = Post::factory(6)->create(['status'=>'PUBLISH','public'=>1,'category_id'=>$this->Category[0],'author_id'=>$this->user[0]->id]);

        $response = $this->get('privadoCategories/'. $this->Category[0]->slug);
        $response->assertStatus(200);
        $response->assertViewHas(['categories','general','link','lpost','posts','recent','tags']);
  
    }

    public function test_tag(){
        Tag::factory(1)->create();
        Link::factory(1)->create();
        Category::factory(1)->create();
        $posts = Post::factory(6)->create(['status'=>'PUBLISH','public'=>1,'category_id'=>$this->Category[0],'author_id'=>$this->user[0]->id]);

        $response = $this->get('privadoCategories/'. $this->Category[0]->slug);
        $response->assertStatus(200);
        $response->assertViewHas(['categories','general','link','lpost','posts','recent','tags']);
  
    }

    public function test_tag_auth(){
        Tag::factory(1)->create();
        Link::factory(1)->create();
        Category::factory(1)->create();
        $posts = Post::factory(6)->create(['status'=>'PUBLISH','public'=>1,'category_id'=>$this->Category[0],'author_id'=>$this->user[0]->id]);

        $response = $this->actingAs($this->user[0])->get('privadoCategories/'. $this->Category[0]->slug);
        $response->assertStatus(200);
        $response->assertViewHas(['categories','general','link','lpost','posts','recent','tags']);
  
    }

    public function test_search_auth(){
        Tag::factory(1)->create();
        Link::factory(1)->create();
        Category::factory(1)->create();
        $posts = Post::factory(6)->create(['status'=>'PUBLISH','public'=>1,'category_id'=>$this->Category[0],'author_id'=>$this->user[0]->id]);

        $response = $this->actingAs($this->user[0])->get('blog/search/');
        $response->assertStatus(200);
        $response->assertViewHas(['categories','general','link','lpost','posts','query','recent','tags']);
  
    }

    public function test_search(){
        Tag::factory(1)->create();
        Link::factory(1)->create();
        Category::factory(1)->create();
        $posts = Post::factory(6)->create(['status'=>'PUBLISH','public'=>1,'category_id'=>$this->Category[0],'author_id'=>$this->user[0]->id]);

        $response = $this->get('blog/search/',[$posts[0]->title]);
        $response->assertStatus(200);
        $response->assertViewHas(['categories','general','link','lpost','posts','query','recent','tags']);
  
    }

    public function test_page(){
        Link::factory(1)->create();
        $posts = Post::factory(6)->create(['status'=>'PUBLISH','public'=>1,'category_id'=>$this->Category[0],'author_id'=>$this->user[0]->id]);

        $response = $this->get('pages/'.$this->Page[0]->slug);
        $response->assertStatus(200);
        $response->assertViewHas(['general','link','lpost','page']);
          
    }

    public function test_subscribe(){
        $subscriber = Subscriber::factory(1)->create();

        $response = $this->post('/',["email"=>"test@test.com"]);
        $response->assertStatus(302);

        $response->assertRedirect('/',['success', 'You have successfully subscribed']);
         
    }

    public function test_subscribe_faild(){
        $subscriber = Subscriber::factory(1)->create();
        $response = $this->post('/',["email"=>$subscriber[0]->email]);
        $response->assertStatus(302);

        $response->assertRedirect(session()->previousUrl());    
    }

    public function test_subscribe_validation(){
        $subscriber = Subscriber::factory(1)->create();
        $response = $this->post('/',[]);
        $response->assertStatus(302);
        $response->assertSessionHasErrors('email');

    }

}
