<?php

namespace Tests\Feature;

use App\Http\Controllers\FrontController;
use App\Models\About;
use App\Models\Banner;
use App\Models\Carnet;
use App\Models\Category;
use App\Models\Certificado;
use App\Models\Cursos;
use App\Models\EntidadesFormadoreas;
use App\Models\Faq;
use App\Models\Formadores;
use App\Models\General;
use App\Models\Link;
use App\Models\Operadores;
use App\Models\Page;
use App\Models\Partner;
use App\Models\Pcategory;
use App\Models\Portfolio;
use App\Models\Post;
use App\Models\Service;
use App\Models\Subscriber;
use App\Models\Tag;
use App\Models\Testimonial;
use App\Models\Tipo_Maquina;
use App\Models\User;
use Carbon\Carbon;
use Database\Factories\cursosFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\ToArray;
use Tests\TestCase;
class GeneralTest extends TestCase
{
    use RefreshDatabase;

    private $General;
    private $Banner;
    private $faq;
    private $partners;
    private $Page;
    private $Testimonial;
    private $Cursos;
    private $EntidadesFormador;
    private $Tipo_Maquina;
    private $service;
    private $pcategory;
    private $portfolios;
    private $Category;
    private $Link;
    private $user;
    private $formadores;
    private $about;

    public function setUp():void{
        parent::setUp();
        
        $this->General = General::factory(1)->create(['id'=>1]); 
        $this->Banner = Banner::factory(1)->create();     
        $this->faq = Faq::factory(1)->create();     
        $this->Page = Page::factory(1)->create();     
        $this->partners = Partner::factory(1)->create();     
        $this->Testimonial = Testimonial::factory(1)->create();
        $this->EntidadesFormador = EntidadesFormadoreas::factory(1)->create(['id'=>2]);
        $this->Cursos = Cursos::factory(1)->create(['entidad'=>2]);
        $this->Tipo_Maquina = Tipo_Maquina::factory(1)->create(); 
        $this->service = Service::factory(1)->create();
        $this->pcategory = Pcategory::factory(1)->create();
        $this->portfolios = Portfolio::factory(1)->create();
        $this->Category = Category::factory(1)->create();
        $this->user = User::factory(12)->create();
        // $this->about = About::factory(1)->create();
        // $this->formadores = Formadores::factory(1)->create();

    }

    public function test_dashboard_Administrador(){
        $user_admin = User::factory(1)->create(['perfil'=>'Administrador']);
        $EntidadesFormador = EntidadesFormadoreas::factory(1)->create(['id'=>1,'estado'=>1]);
        Formadores::factory(1)->create(['entidad'=>$EntidadesFormador[0]->id,'estado'=>1]);
        $operador = Operadores::factory(1)->create(['estado'=>1]);
       $curso =  Cursos::factory(1)->create(['estado'=>1]);

        $response = $this->actingAs($user_admin[0])->get('admin/dashboard');
        $response->assertStatus(200);

        $response->assertViewHas('admin');
        
        $response->assertViewHas('operador');

        $response->assertViewHas('activo_Curso');

        $response->assertViewHas('formadores');

        $response->assertViewHas('last_curso', function ($collection) use($curso) {
            return $collection->contains($curso[0]);
        });
        $response->assertViewHas('entidad', function ($collection) use($EntidadesFormador){
            return $collection->contains($EntidadesFormador[0]);
        });


    }

    public function test_dashboard_Formador(){
        $user_Formador = User::factory(1)->create(['perfil'=>'Formador','estado'=>1]);
        $user = User::factory(1)->create(['estado'=>1,'entidad'=>$user_Formador[0]->entidad]);
        $EntidadesFormador = EntidadesFormadoreas::factory(1)->create(['id'=>$user_Formador[0]->entidad,'estado'=>1]);       
        $formadores = Formadores::factory(1)->create(['estado'=>1,'entidad'=>$EntidadesFormador[0]->id]);
        
        $operador = Operadores::factory(1)->create(['estado'=>1,'entidad'=>$user_Formador[0]->entidad]);
        $curso =  Cursos::factory(1)->create(['estado'=>1,'entidad'=>$user_Formador[0]->entidad]);
       
        $response = $this->actingAs($user[0])->get('admin/dashboard');
        $response->assertStatus(200);
        
        $response->assertViewHas('admin');
        
        $response->assertViewHas('operador');

        $response->assertViewHas('activo_Curso');

        $response->assertViewHas('formadores');

        $response->assertViewHas('last_curso', function ($collection) use($curso) {
            return $collection->contains($curso[0]);
        });
        $response->assertViewHas('entidad', function ($collection) use($EntidadesFormador){
            return $collection->contains($EntidadesFormador[0]);
        });

    }

    public function test_general(){
        $user = User::factory(1)->create(['perfil'=>'Administrador']);
        $response = $this->actingAs($user[0])->get('admin/general-settings');

        $response->assertStatus(200);
        
        $response->assertViewHas('general');

    }

    public function test_generalUpdate(){
        Storage::fake('logo');

        General::factory(1)->create(['title'=>'test']);
        $user = User::factory(1)->create(['perfil'=>'Administrador']);
        $response = $this->actingAs($user[0])->post('admin/general-settings',[
            "title" => $this->General[0]->title,
            "address1" => $this->General[0]->address1,
            "address2" => $this->General[0]->address2,
            "phone" => $this->General[0]->phone ,
            "email" => $this->General[0]->email,
            "twitter"=>$this->General[0]->twitter,
            "facebook"=>$this->General[0]->facebook,
            "instagram"=>$this->General[0]->instagram,
            "linkedin"=>$this->General[0]->linkedin,
            "footer"=>$this->General[0]->footer,
            "gmaps"=>$this->General[0]->gmaps,
            "tawkto"=>$this->General[0]->tawkto,
            "disqus"=>$this->General[0]->disqus,
            "sharethis"=>$this->General[0]->sharethis,
            "gverification"=>$this->General[0]->gverification,
            "keyword"=>$this->General[0]->keyword,
            "meta_desc"=>$this->General[0]->meta_desc,
            "logo" => UploadedFile::fake()->create('logo.jpg'),
            "favicon" => UploadedFile::fake()->create('logo.jpg'),
        ]);

        $response->assertStatus(302);

        $response->assertSessionHas('success','Data updated successfully');
    }

    public function test_about(){
        $about= About::factory(1)->create(["id"=>1]);
        $user = User::factory(1)->create(['perfil'=>'Administrador']);
        $response = $this->actingAs($user[0])->get('admin/about');
        $response->assertStatus(200);

        $response->assertSee('about');

    }

    public function test_aboutUpdate(){
        $about= About::factory(1)->create(["id"=>1]);

        $user = User::factory(1)->create(['perfil'=>'Administrador']);
        $response = $this->actingAs($user[0])->post('admin/about',[
            "title" => $about[0]->title,
            "subject" => $about[0]->subject,
            "desc" => $about[0]->desc,
        ]);
        $response->assertStatus(302);

        $response->assertRedirect('admin/about');
    }
}
