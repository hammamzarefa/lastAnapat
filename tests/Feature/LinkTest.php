<?php

namespace Tests\Feature;


use App\Http\Controllers\FrontController;
use App\Models\About;
use App\Models\Banner;
use App\Models\Carnet;
use App\Models\Category;
use App\Models\Certificado;
use App\Models\Cursos;
use App\Models\EntidadesFormadoreas;
use App\Models\Faq;
use App\Models\Formadores;
use App\Models\General;
use App\Models\Horario;
use App\Models\Link;
use App\Models\Operadores;
use App\Models\Page;
use App\Models\Partner;
use App\Models\Pcategory;
use App\Models\Portfolio;
use App\Models\Post;
use App\Models\Service;
use App\Models\Subscriber;
use App\Models\Tag;
use App\Models\Testimonial;
use App\Models\Tipo_Maquina;
use App\Models\User;
use Carbon\Carbon;
use Database\Factories\cursosFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\ToArray;
use Tests\TestCase;

class LinkTest extends TestCase
{
    use RefreshDatabase;

    private $General;
    private $Banner;
    private $faq;
    private $partners;
    private $Page;
    private $Testimonial;
    private $Cursos;
    private $EntidadesFormador;
    private $Tipo_Maquina;
    private $service;
    private $pcategory;
    private $portfolios;
    private $Category;
    private $Link;
    private $user;
    private $formadores;
    private $about;
    private $horario;

    public function setUp():void{
        parent::setUp();
        
        $this->General = General::factory(1)->create(['id'=>1]); 
        $this->Banner = Banner::factory(1)->create();     
        $this->faq = Faq::factory(1)->create();     
        $this->Page = Page::factory(1)->create();     
        $this->partners = Partner::factory(1)->create();     
        $this->Testimonial = Testimonial::factory(1)->create();
        $this->EntidadesFormador = EntidadesFormadoreas::factory(1)->create(['id'=>2]);
        $this->Cursos = Cursos::factory(1)->create(['entidad'=>2]);
        $this->Tipo_Maquina = Tipo_Maquina::factory(1)->create(); 
        $this->service = Service::factory(1)->create();
        $this->pcategory = Pcategory::factory(1)->create();
        $this->portfolios = Portfolio::factory(1)->create();
        $this->Category = Category::factory(1)->create();
        $this->user = User::factory(12)->create();
        $this->horario = Horario::factory(1)->create();
        $this->Link = Link::factory(1)->create();
        // $this->about = About::factory(1)->create();
        // $this->formadores = Formadores::factory(1)->create();

    }

    public function test_index()
    {
        $response = $this->actingAs($this->user[0])->get('admin/abouts');

        $response->assertStatus(200);
        $response->assertViewHas('link', function ($collection)  {
            return $collection->contains($this->Link[0]);
        });
    }

    public function test_create(){
        $response = $this->actingAs($this->user[0])->get('admin/abouts/create/');
        $response->assertStatus(200);    
    }

    public function test_store(){
        $response = $this->actingAs($this->user[0])->post('admin/abouts/create',[
            'título' => 'required',
            'descripción' => 'required',
            'enlace' => 'required',
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('success', 'Data added successfully');

    }

    public function test_edit(){
        $response = $this->actingAs($this->user[0])->get('admin/abouts/edit/'. $this->Link[0]->id);
        $response->assertStatus(200);
    }

    public function test_update(){
        $response = $this->actingAs($this->user[0])->post('admin/abouts/edit/'. $this->Link[0]->id,[
            'título' => 'required',
            'descripción' => 'required',
            'enlace' => 'required',
        ]);
        $response->assertStatus(302);
        $response->assertSessionHas('success', 'Data updated successfully');
    }

    public function test_destroy(){
        $response = $this->actingAs($this->user[0])->delete('admin/abouts/destroy/'. $this->Link[0]->id);
        $response->assertStatus(302);
        $response->assertSessionHas('success', 'Data deleted successfully');
    }
}
